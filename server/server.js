// server.js

var bodyParser = require("body-parser");
var express = require('express');
var request = require('request');
var os = require('os');

var port = 6000;
var app = express();
var server = app.listen(process.env.PORT || port);
var io = require('socket.io').listen(server);


app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use('/', express.static(__dirname));
//app.set("content", __dirname + '/content');



// ROUTES
// ==============================================

app.post('/receive', function(req, res) {
  console.log("New notification from SmartSantander received!!");
    var rawvalue = JSON.parse(JSON.stringify(req.body, null, 2));
    var name = rawvalue.urn;
    var value = rawvalue.value;
    var timestamp = Math.floor(new Date(rawvalue.timestamp).getTime() / 1000);

    var smartcampusobject = {};
    smartcampusobject["n"] = name;
    smartcampusobject["v"] = value.toString();
    smartcampusobject["t"] = timestamp.toString();

    var formData = JSON.stringify(smartcampusobject);
    console.log(formData);

    request({
        url: "http://localhost:8080/collector/value/",
        method: "POST",
        headers: {
            "content-type": "application/json",
            "accept": "text/plain"
        },
        json: smartcampusobject
    }, function (error, resp, body) {});
  res.send('ok');
});

////////////////////////// End Routing /////////////////////////////////////

// START THE SERVER
// ==============================================

var interfaces = os.networkInterfaces();
var addresses = [];
for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family === 'IPv4' && !address.internal) {
            addresses.push(address.address);
        }
    }
}
console.log('Server launched on http://' + addresses[0] + ":" + port);