#!/bin/bash

sudo apt-get update
curl -sL https://deb.nodesource.com/setup_0.12 | sudo bash -
sudo apt-get install -y nodejs
cd server
npm install
echo 'Installation finished!!'
echo 'Just run "node server.js" on server folder'